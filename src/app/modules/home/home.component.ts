import { Component } from '@angular/core';

// interface
import { RaceService } from '../../shared/services/race.service';

// external libs
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  resultsView = [];
  orderBy = 'ASC';
  invalidSearch = false;

  constructor( private raceService: RaceService, private spinner: NgxSpinnerService ) { }

  /**
   * Function to return results from API
   * @param {Array<String>} emittedValue
   */
  getResults(emittedValue: Array<String>) {
    const year = emittedValue[0];
    const round = emittedValue[1];

    /* if it is empty it will not continue  */
    if ( year.length < 4 || round === '') {
      return;
    }

    this.spinner.show();
    this.resultsView = [];
    this.orderBy = 'ASC';
    this.invalidSearch = false;

    this.raceService.getRaceResults(year, round).subscribe(
      data => {
        this.spinner.hide();

        if (!data.MRData.RaceTable.Races.length) {
          this.invalidSearch = true;
          return;
        }

        this.resultsView = data.MRData.RaceTable.Races;
      },
      err => {
        this.spinner.hide();
        console.log('error API');
      }
    );

  }

  /**
   * function to order results for ASC or DESC
   * @param {string} option
   */
  sortBy(option: string) {
    if ( option === 'DESC' ) {
      this.resultsView[0].Results.sort((a, b) => parseFloat(b.position) - parseFloat(a.position));
    } else {
      this.resultsView[0].Results.sort((a, b) => parseFloat(a.position) - parseFloat(b.position));
    }

  }

}
