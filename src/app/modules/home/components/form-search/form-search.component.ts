import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form-search',
  templateUrl: './form-search.component.html',
  styleUrls: ['./form-search.component.scss']
})
export class FormSearchComponent {
  formValues = [];

  @Output() sendSearch = new EventEmitter();

  constructor() { }

  /**
   * Function to send emitter to filter search
   * @param {string} option
   */
  emitterSearchFilter(e: Event, year: String, round: String) {
    e.preventDefault();
    this.formValues = [year, round];
    this.sendSearch.emit(this.formValues);
  }

}
