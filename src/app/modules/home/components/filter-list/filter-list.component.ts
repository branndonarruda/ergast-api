import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter-list',
  templateUrl: './filter-list.component.html',
  styleUrls: ['./filter-list.component.scss']
})
export class FilterListComponent {
  orders = ['ASC', 'DESC'];

  @Input() orderBy: string;
  @Output() sendAction = new EventEmitter();

  constructor() { }

  /**
   * Function to send emitter to filter list
   * @param {string} option
   */
  emitterOrderBy(option) {
    this.sendAction.emit(option);
  }

}
