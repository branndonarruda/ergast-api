# iCarros - Ergast API
Aplicativo baseado no Ergast Developer API um serviço web experimental que fornece um registro histórico de dados de corridas de automóveis para fins não comerciais.

Caso queira ve-lo em produção, [veja uma demo ](http://branndonarruda.bitbucket.io/ergast-api/) aqui.
### Pre requisitos
- Node >= 11.14.0
- Angular 6.1.10
- Angular CLI: 6.1.5

## 1 - Baixe o Projeto
Faça o Download do Projeto ou clone ele
```cmd
git clone https://branndonarruda@bitbucket.org/branndonarruda/ergast-api.git
```

## 2 - Instale as Dependências
depois de baixar/clonar, entre dentro da pasta do projeto, e instale as dependências com o seguinte comando:
```cmd
npm install
```
## 3 - Rode o Projeto
Para dar build no projeto e visualizar a webapp em seu browser, rode o seguinte comando:
```cmd
ng serve
```
A webapp irá executar na porta 4200 do seu navegador.

## 4 - Finalizado
Para acessar a aplicação, acesse o endereço abaixo no seu navegador:
[http://localhost:4200](http://localhost:4200)

## 5 - Gerando o DIST para produção
Para gerar o DIST minificado para produção, em seu terminal rode o seguinte comando:
```cmd
ng build --base-href https://url-base.com --prod
```