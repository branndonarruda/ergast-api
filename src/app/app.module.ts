import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './modules/home/home.component';
import { routing } from './app.routes';

// external libs
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxMaskModule } from 'ngx-mask';
import { TableResultComponent } from './modules/home/components/table-result/table-result.component';
import { FilterListComponent } from './modules/home/components/filter-list/filter-list.component';
import { FormSearchComponent } from './modules/home/components/form-search/form-search.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TableResultComponent,
    FilterListComponent,
    FormSearchComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpModule,
    FormsModule,
    NgxSpinnerModule,
    NgxMaskModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
