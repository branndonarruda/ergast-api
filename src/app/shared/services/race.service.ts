import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

import { ResultInterface } from '../interface/race.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RaceService {

    constructor( private http: Http ) {}

    getRaceResults(year: String, round: String): Observable<ResultInterface> {
        return this.http.get('https://ergast.com/api/f1/' + year + '/' + round + '/results.json').pipe(
            map(response => response.json())
        );
    }
}
